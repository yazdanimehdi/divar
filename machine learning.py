import pandas as pd
import numpy as np

import pickle
from sklearn.feature_extraction.text import CountVectorizer

df = pd.read_csv('divar_posts_dataset.csv')
category = list()
for item in df['cat1']:
    if item in category:
        continue
    else:
        category.append(item)


df["cat2"] = df["cat2"].fillna('0')
df["cat3"] = df["cat3"].fillna('0')
sentences = list()
labels = list()
for item in df.iterrows():
    sentences.append(item[1]['title'] + ' ' + item[1]['desc'])
    labels.append(item[1]['cat1'])

sentences_train, sentences_test, y_train, y_test = train_test_split(sentences, labels, test_size=0.001)

vectorizer = CountVectorizer()
vectorizer.fit(sentences_train)

X_train = vectorizer.transform(sentences_train)
X_test = vectorizer.transform(sentences_test)
classifier = SVC()
classifier.fit(X_train, y_train)
score = classifier.score(X_test, y_test)
print("Accuracy:", score)
out = pd.read_csv('phase_2_dataset.csv')
out['desc'] = out['desc'].astype(str)
out['title'] = out['title'].astype(str)
sentences = list()
labels = list()
for item in out.iterrows():
    sentences.append(item[1]['title'] + ' ' + item[1]['desc'])
X = vectorizer.transform(sentences)
y = classifier.predict(X)
out['cat1'] = y
out['cat2'] = np.nan
out['cat3'] = np.nan
lst = [out]
for col in lst:
    col.loc[col['cat1'] == 1, "cat1"] = 'for-the-home'
    col.loc[col['cat1'] == 2, "cat1"] = 'vehicles'
    col.loc[col['cat1'] == 3, "cat1"] = 'personal'
    col.loc[col['cat1'] == 4, "cat1"] = 'electronic-devices'
    col.loc[col['cat1'] == 5, "cat1"] = 'businesses'
    col.loc[col['cat1'] == 6, "cat1"] = 'leisure-hobbies'

for cat in category:
    cat2 = df.loc[df['cat1'] == cat]
    sentences = list()
    labels = list()
    for item in cat2.iterrows():
        sentences.append(item[1]['title'] + ' ' + item[1]['desc'])
        labels.append(item[1]['cat2'])

    sentences_train, sentences_test, y_train, y_test = train_test_split(sentences, labels, test_size=0.001)
    X_train = vectorizer.transform(sentences_train)
    X_test = vectorizer.transform(sentences_test)
    if X_train != [] and y_train != []:
        classifier = SVC()
        classifier.fit(X_train, y_train)
        score = classifier.score(X_test, y_test)
        print("Accuracy:", score)
        sentences = list()
        labels = list()
        out2 = out.loc[out['cat1'] == cat]
        for item in out2.iterrows():
            sentences.append(item[1]['title'] + ' ' + item[1]['desc'])
        X = vectorizer.transform(sentences)
        y = classifier.predict(X)
        out.loc[out['cat1'] == cat, 'cat2'] = y


category = list()
for item in df['cat2']:
    if item in category:
        continue
    else:
        category.append(item)

for cat in category:
    cat2 = df.loc[df['cat2'] == cat]
    sentences = list()
    labels = list()
    for item in cat2.iterrows():
        sentences.append(item[1]['title'] + ' ' + item[1]['desc'])
        labels.append(item[1]['cat3'])

    sentences_train, sentences_test, y_train, y_test = train_test_split(sentences, labels, test_size=0.001)
    X_train = vectorizer.transform(sentences_train)
    X_test = vectorizer.transform(sentences_test)

    ytrain = list()
    for item in y_train:
        if item in ytrain:
            continue
        else:
            ytrain.append(item)
    if X_train != [] and y_train != [] and len(ytrain) > 1:
        classifier = LogisticRegression()
        classifier.fit(X_train, y_train)
        score = classifier.score(X_test, y_test)
        print("Accuracy:", score)
        sentences = list()
        labels = list()
        out2 = out.loc[out['cat2'] == cat]
        for item in out2.iterrows():
            sentences.append(item[1]['title'] + ' ' + item[1]['desc'])
        X = vectorizer.transform(sentences)
        y = classifier.predict(X)
        out.loc[out['cat2'] == cat, 'cat3'] = y

a = out[['cat1', 'cat2', 'cat3']]
a.to_csv('our1.csv')